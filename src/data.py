from dataclasses import dataclass
from typing import Callable


@dataclass
class Moiety:
    func: Callable
    derived_variable: str
    compounds: list[str]
    modifiers: list[str]
    parameters: list[str]
    args: list[str]


@dataclass
class Rate:
    func: Callable
    substrates: list[str]
    products: list[str]
    modifiers: list[str]
    parameters: list[str]
    reversible: bool
    args: list[str]
