from .data import Moiety, Rate
from .utils import unpack_stoichiometries
from typing import Callable, Optional


def generate_substrates(base_substrate: str, suffixes: list[str]) -> list[str]:
    return [f"{base_substrate}_{i}" for i in suffixes]


def generate_all_substrates(
    substrates: list[str],
    base_suffixes: list[str],
    special_suffixes: Optional[dict[str, list[str]]] = None,
) -> list[list[str]]:
    if special_suffixes is None:
        special_suffixes = {}
    return [
        generate_substrates(
            base_substrate,
            special_suffixes.get(base_substrate, base_suffixes),
        )
        for base_substrate in substrates
    ]


def create_rate_function_name(
    n_substrate_cofactors: int,
    n_product_cofactors: int,
    n_substrate_competitors: int,
    n_product_competitors: int,
    n_inhibitors: int,
) -> str:
    return (
        "enzyme_"
        f"{n_substrate_cofactors}scf_"
        f"{n_product_cofactors}pcf_"
        f"{n_substrate_competitors}scp_"
        f"{n_product_competitors}pcp_"
        f"{n_inhibitors}i"
    )


def create_rate_function(
    n_substrate_cofactors: int,
    n_product_cofactors: int,
    n_substrate_competitors: int,
    n_product_competitors: int,
    n_inhibitors: int,
) -> str:
    """
    Ordering:
        - substrate
        - product
        - (substrate cofactors)
        - (product cofactors)
        - vmax
        - sf
        - keq
        - km substrates
        - km products
        - km cofactors
        - km substrate competitors
        - km product competitors
        - ki inhibitors
    """

    function_name = create_rate_function_name(
        n_substrate_cofactors,
        n_product_cofactors,
        n_substrate_competitors,
        n_product_competitors,
        n_inhibitors,
    )

    args = ["s1", "p1"]
    parameters = ["vmax", "sf", "keq", "km_s1", "km_p1"]

    cofactors = []
    for i in range(1, n_substrate_cofactors + 1):
        args.append(f"scf_{i}")
        parameters.append(f"km_scf_{i}")
        cofactors.append([f"scf_{i}", f"km_scf_{i}"])

    for i in range(1, n_product_cofactors + 1):
        args.append(f"spf_{i}")
        parameters.append(f"km_spf_{i}")
        cofactors.append([f"scf_{i}", f"km_scf_{i}"])

    competitors = []
    for i in range(1, n_substrate_competitors + 1):
        args.append(f"scp_{i}")
        parameters.append(f"km_scp_{i}")
        competitors.append([f"scp_{i}", f"km_scp_{i}"])

    for i in range(1, n_product_competitors + 1):
        args.append(f"pcp_{i}")
        parameters.append(f"km_pcp_{i}")
        competitors.append([f"pcp_{i}", f"km_pcp_{i}"])

    inhibitors = []
    for i in range(1, n_inhibitors + 1):
        args.append(f"i_{i}")
        parameters.append(f"ki_{i}")
        inhibitors.append([f"i_{i}", f"ki_{i}"])

    if len(competitors) == 0:
        fn_competitors = ""
    else:
        gen = " + ".join([f"{i[0]} / {i[1]}" for i in competitors])
        fn_competitors = f"+ {gen}"
    if len(inhibitors) == 0:
        fn_inhibitors = ""
    else:
        gen = " + ".join([f"{i[0]} / {i[1]}" for i in inhibitors])
        fn_inhibitors = f"+ {gen}"
    if len(cofactors) == 0:
        fn_cofactors = ""
    else:
        gen = " + ".join([f"{i[0]} / {i[1]}" for i in cofactors])
        fn_cofactors = f"* ({gen})"

    fn_args = ",\n    ".join(args + parameters)
    fn_body = f"""
    numerator = vmax * sf * (s1 / km_s1 - p1 / (km_p1 * keq))
    denominator = (1 + (s1 / km_s1 + p1 / km_p1 {fn_competitors} {fn_inhibitors})) {fn_cofactors}
    return numerator / denominator
    """

    return f"""def {function_name}(
    {fn_args}
):
    {fn_body}
"""


def get_compounds_by_suffix(all_compounds: list[str]) -> dict[str, list[str]]:
    cpds_by_suffix: dict[str, list[str]] = {}
    for compound in all_compounds:
        _, suffix = compound.rsplit("_", maxsplit=1)
        cpds_by_suffix.setdefault(suffix, []).append(compound)
    return cpds_by_suffix


def create_stoichiometries(
    all_compounds: list[str],
    substrate_enzymes: dict[str, list[str]],
    enzyme_products: dict[str, str],
    forbidden_substrates: dict[str, list[str]],
) -> dict[str, dict[str, float]]:
    stoichiometries_by_compounds: dict[str, dict[str, float]] = {}
    for substrate in all_compounds:
        base, suffix = substrate.split("_", maxsplit=1)
        for enzyme in substrate_enzymes[suffix]:
            if base in forbidden_substrates.get(enzyme, []):
                continue
            product = f"{base}_{enzyme_products[enzyme]}"
            if substrate == product:
                raise ValueError(f"{substrate} for enzyme {enzyme}")
            stoichiometry = {substrate: -1.0, product: 1.0}
            stoichiometries_by_compounds[f"v{enzyme}_{base}"] = stoichiometry
    return stoichiometries_by_compounds


def generate_reaction_parameter_names(
    enzyme: str,
    stoichiometries: dict[str, float],
    no_sf: Optional[list[str]] = None,
) -> list[str]:
    """
    Ordering
        - vmax
        - sf
        - km substrates
        - km products
    """
    parameters = []
    if no_sf is None:
        no_sf = []
    base_enzyme_name = enzyme[1:].rsplit("_", maxsplit=1)[0]

    # vmax
    parameters.append(f"vmax_{base_enzyme_name}")
    substrates, products = unpack_stoichiometries(stoichiometries)

    # sf (optional)
    for substrate in substrates:
        if base_enzyme_name not in no_sf:
            parameters.append(f"sf_{base_enzyme_name}_{substrate}")

    # keq
    parameters.append(f"keq_{base_enzyme_name}")

    # km substrates
    for substrate in substrates:
        parameters.append(f"km_{base_enzyme_name}_{substrate}")

    # km products
    for product in products:
        parameters.append(f"km_{base_enzyme_name}_{product}")
    return parameters


# def generate_parameters(
#     stoichiometries: dict[str, dict[str, float]],
#     no_sf: Optional[list[str]] = None,
# ) -> dict[str, float]:
#     # TODO: also add other parameters
#     if no_sf is None:
#         no_sf = []
#     parameters: dict[str, float] = {}
#     for enzyme, stoich in stoichiometries.items():
#     return parameters


def generate_rates(
    functions: dict[str, Callable],
    cpds_by_suffix: dict[str, list[str]],
    stoichiometries_by_rate: dict[str, dict[str, float]],
    inhibitors: dict[str, list[str]],
    cofactors: dict[str, dict[str, list[str]]],
) -> dict[str, Rate]:

    rates = {}
    for reaction_name, stoich in stoichiometries_by_rate.items():
        rate_parameters = generate_reaction_parameter_names(reaction_name, stoich)

        substrates, products = unpack_stoichiometries(stoich)

        # TODO: general case where you might have more substrates / compounds
        substrate_competitors = cpds_by_suffix[substrates[0].split("_")[1]]
        product_competitors = cpds_by_suffix[products[0].split("_")[1]]

        substrate_cofactors = cofactors.get(reaction_name, {}).get("substrate", [])
        product_cofactors = cofactors.get(reaction_name, {}).get("product", [])

        rate_inhibitors = inhibitors.get(reaction_name, [])
        rate_competitors = substrate_competitors + product_competitors

        function_name = create_rate_function_name(
            n_substrate_cofactors=len(substrate_cofactors),
            n_product_cofactors=len(product_cofactors),
            n_substrate_competitors=len(substrate_competitors),
            n_product_competitors=len(product_competitors),
            n_inhibitors=len(rate_inhibitors),
        )

        inhibitor_parameters = [f"ki_{i}" for i in rate_inhibitors]
        competitor_parameters = [f"km_{i}" for i in rate_competitors]

        parameters = rate_parameters + inhibitor_parameters + competitor_parameters
        modifiers = substrate_competitors + product_competitors + rate_inhibitors

        args = substrates + products + modifiers + parameters

        rates[reaction_name] = Rate(
            func=functions[function_name],
            substrates=substrates,
            products=products,
            modifiers=modifiers,
            parameters=parameters,
            reversible=True,
            args=args,
        )
    return rates


def generate_moieties() -> dict[str, Moiety]:
    # TODO: Add this
    ...
