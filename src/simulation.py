import itertools as it
from .data import Moiety, Rate
from typing import Callable, Iterable, Iterator, ValuesView


def get_fcd(
    moieties: dict[str, Moiety],
    args: dict[str, float],
) -> dict[str, float]:
    # TODO: Doesn't allow for moieties dependent on each other, that's an exercise for you
    for v in moieties.values():
        args[v.derived_variable] = v.func(*(args[i] for i in v.args))
    return args


def get_rates(
    rates: dict[str, Rate],
    args: dict[str, float],
) -> dict[str, float]:
    return {k: v.func(*(args[i] for i in v.args)) for k, v in rates.items()}


def get_rhs(
    y: dict[str, float],
    p: dict[str, float],
    moieties: dict[str, Moiety],
    rates: dict[str, Rate],
    stoich_by_cpd: dict[str, dict[str, float]],
) -> dict[str, float]:
    args: dict[str, float] = {**y, **p}
    args = get_fcd(moieties=moieties, args=args)
    v = get_rates(rates=rates, args=args)

    dxdt: dict[str, float] = dict(zip(stoich_by_cpd, it.repeat(0.0)))
    for cpd, stochiometry in stoich_by_cpd.items():
        for rate_name, factor in stochiometry.items():
            dxdt[cpd] += factor * v[rate_name]
    return dxdt


def get_model(
    p: dict[str, float],
    moieties: dict[str, Moiety],
    rates: dict[str, Rate],
    stoich_by_cpd: dict[str, dict[str, float]],
) -> Callable[[float, Iterable[float]], ValuesView[float]]:
    return lambda t, y: get_rhs(
        dict(zip(stoich_by_cpd, y)), p, moieties, rates, stoich_by_cpd
    ).values()
