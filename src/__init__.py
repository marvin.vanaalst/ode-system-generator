from .data import Moiety, Rate
from .generator import (
    create_rate_function,
    create_rate_function_name,
    create_stoichiometries,
    generate_all_substrates,
    generate_moieties,
    generate_rates,
    generate_reaction_parameter_names,
    generate_substrates,
    get_compounds_by_suffix,
)
from .simulation import get_fcd, get_model, get_rates, get_rhs
from .utils import (
    flatten,
    head,
    invert_stoichiometries,
    stringify_nested_list,
    unpack_stoichiometries,
)
from modelbase.ode import Model as _Model


def to_modelbase(
    compounds: list[str],
    parameters: dict[str, float],
    moieties: dict[str, Moiety],
    rates: dict[str, Rate],
    stoichiometries: dict[str, dict[str, float]],
) -> _Model:
    m = _Model()
    m.add_compounds(compounds)
    m.add_parameters(parameters)
    for module_name, module in moieties.items():
        m.add_algebraic_module(
            module_name=module_name,
            function=module.func,
            derived_compounds=[module.derived_variable],
            compounds=module.compounds,
            modifiers=module.modifiers,
            parameters=module.parameters,
            args=module.args,
        )

    for rate_name, rate in rates.items():
        m.add_rate(
            rate_name=rate_name,
            function=rate.func,
            substrates=rate.substrates,
            products=rate.products,
            modifiers=rate.modifiers,
            parameters=rate.parameters,
            reversible=rate.reversible,
            args=rate.args,
        )

    m.add_stoichiometries(stoichiometries)
    return m
